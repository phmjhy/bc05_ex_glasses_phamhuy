import React, { Component } from "react";

export default class DetailGlass extends Component {
    render() {
        return (
            <div>
                <div
                    className="position-absolute"
                    style={{
                        top: "70%",
                        left: "10%",
                        width: "34.2%",
                        height: "25%",
                        backgroundColor: "#E0FFFF",
                        textAlign: "left",
                    }}
                >
                    <img
                        style={{
                            marginTop: "-65rem",
                            marginLeft: "22.5rem",
                            width: "240px",
                            height: "30%",
                            opacity: "0.8",
                        }}
                        src={this.props.detail.url}
                        alt=""
                    />
                    <p>
                        {this.props.detail.name}
                    </p>
                    <p>
                        {this.props.detail.desc}
                    </p>
                </div>
            </div>
        );
    }
}
